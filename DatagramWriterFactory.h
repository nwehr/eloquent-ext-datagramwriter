#ifndef __eloquent__UDPWriterFactory__
#define __eloquent__UDPWriterFactory__

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <syslog.h>

// C++
#include <string>
#include <vector>

// Boost
#include <boost/thread.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/property_tree/ptree.hpp>

// Internal
#include "Eloquent/Extensions/IO/IOFactory.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// DatagramWriterFactory : IOExtensionFactory
	///////////////////////////////////////////////////////////////////////////////
	class DatagramWriterFactory : public IOFactory {
	public:
		DatagramWriterFactory();
		virtual ~DatagramWriterFactory();
		
		virtual IO* New( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , unsigned int& i_NumWriters );
		
	};
	
}

#endif /* defined(__eloquent__UDPWriterFactory__) */