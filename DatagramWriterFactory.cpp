//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// Internal
#include "DatagramWriter.h"
#include "DatagramWriterFactory.h"

///////////////////////////////////////////////////////////////////////////////
// DatagramWriterFactory : IOExtensionFactory
///////////////////////////////////////////////////////////////////////////////
Eloquent::DatagramWriterFactory::DatagramWriterFactory() {}
Eloquent::DatagramWriterFactory::~DatagramWriterFactory() {}
	
Eloquent::IO* Eloquent::DatagramWriterFactory::New( const boost::property_tree::ptree::value_type& i_Config
															, std::mutex& i_QueueMutex
															, std::condition_variable& i_QueueCV
															, std::queue<QueueItem>& i_Queue
															, unsigned int& i_NumWriters )
{
	return new DatagramWriter( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters );
}