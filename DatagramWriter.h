#ifndef _DatagramWriter_h
#define _DatagramWriter_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <syslog.h>

// C++
#include <iostream>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/asio.hpp>

// Internal
#include "Eloquent/Extensions/IO/IO.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// DatagramWriter : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class DatagramWriter : public IO {
		DatagramWriter();
		
	public:
		explicit DatagramWriter( const boost::property_tree::ptree::value_type& i_Config
								, std::mutex& i_QueueMutex
								, std::condition_variable& i_QueueCV
								, std::queue<QueueItem>& i_Queue
								, unsigned int& i_NumWriters );

		virtual ~DatagramWriter();
		virtual void operator()();

	private:
		// Networking
		boost::asio::io_service* m_IOService;
		boost::asio::ip::udp::socket* m_Socket;

	};
	
}

#endif // _UDPWriter_h
