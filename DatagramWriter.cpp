//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <ctime>
#include <mutex>

// Boost
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

// Internal
#include "DatagramWriter.h"

///////////////////////////////////////////////////////////////////////////////
// UDPWriter : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::DatagramWriter::DatagramWriter( const boost::property_tree::ptree::value_type& i_Config
										 , std::mutex& i_QueueMutex
										 , std::condition_variable& i_QueueCV
										 , std::queue<QueueItem>& i_Queue
										 , unsigned int& i_NumWriters )
: IO( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_IOService( 0 )
, m_Socket( 0 )
{
	m_IOService = new boost::asio::io_service();
}

Eloquent::DatagramWriter::~DatagramWriter() {
	delete m_IOService;
	delete m_Socket;
}

void Eloquent::DatagramWriter::operator()() {
	try {
		boost::asio::ip::udp::resolver Resolver( *m_IOService );
		
		boost::asio::ip::udp::resolver::query Query( boost::asio::ip::udp::v4()
													, m_Config.second.get<std::string>( "host" )
													, m_Config.second.get<std::string>( "port" ) );
		
		boost::asio::ip::udp::endpoint Endpoint = *Resolver.resolve( Query );
		
		m_Socket = new boost::asio::ip::udp::socket( *m_IOService );
		
		int NumExceptions = 0;
		std::time_t LastExceptionTimestamp;
		
		while( true ) {
			try {
				QueueItem& Item = NextQueueItem();
				
				// Repoen the socket if necessary
				if( !m_Socket->is_open() ){
					m_Socket->open( boost::asio::ip::udp::v4() );
				}
				
				std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
				m_Socket->send_to( boost::asio::buffer( Item.Data() ), Endpoint );
				
			} catch( std::exception& e ) {
				std::time_t CurrentExceptionTimestamp = std::time( 0 );
				
				if( (CurrentExceptionTimestamp - LastExceptionTimestamp) < 1000 ) {
					++NumExceptions;
				} else {
					NumExceptions = 1;
				}
				
				syslog( LOG_ERR, "%s #Error #Writer #DatagramWriter", e.what() );
				
				if( NumExceptions > 4 ) {
					syslog( LOG_ERR, "too many errors, exiting writer #Error #Writer #DatagramWriter" ); break;
				}
				
			} catch( ... ) {
				syslog( LOG_ERR, "unknown exception, exiting writer #Error #Writer #DatagramWriter" ); break;
			}
			
		}
		
	} catch( std::exception& e ) {
		syslog( LOG_ERR, "%s, exiting writer #Error #Writer #DatagramWriter", e.what() );
	} catch( ... ) {
		syslog( LOG_ERR, "unknown exception, exiting writer #Error #Writer #DatagramWriter" );
	}

	delete this;

}